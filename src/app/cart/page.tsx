"use client";

import React from "react";
import Link from "next/link";
import Image from "next/image";
import Button from "@/components/Button/Button";
import { productsProps } from "@/types";
import { useSelector, useDispatch } from "react-redux";
import {
  incrementQuantity as incrementQuantityAction,
  decrementQuantity as decrementQuantityAction,
} from "@/redux/cart.slice";

const Cart = () => {
  const cart = useSelector((state: any) => state.cart);
  const dispatch = useDispatch();

  const getTotalPrice = () => {
    return cart.reduce(
      (accumulator: number, item: any) =>
        Math.round((accumulator + item.quantity * item.price) * 100) / 100,
      0
    );
  };

  return (
    <div className="pt-44 pb-32 text-black shadow-md flex justify-center">
      {cart.length === 0 ? (
        <div className=" flex flex-col justify-center items-center">
          <h1>Your Cart is Empty..</h1>
          <Link
            href="/"
            className=" bg-orange-300 text-white font-semibold rounded-lg mt-3 p-2 "
          >
            Go to Shop
          </Link>
        </div>
      ) : (
        <div className="flex flex-col gap-10 max-w-screen-xl ">
          <div className="flex w-full justify-evenly text-center">
            <div className=" w-80 ">Product</div>
            <div className=" w-80">Price</div>
            <div className=" w-80">Quantity</div>
            <div className=" w-80">Total Price</div>
          </div>
          <hr></hr>
          {cart.map((product: productsProps) => {
            return (
              <div
                className="flex w-full justify-evenly text-center items-center"
                key={product.id}
              >
                <Link href={`/product/${product.id}`}>
                  <div className=" w-80 flex max-sm:flex-col max-md:flex-col max-sm:w-20 max-md:w-24 items-center justify-start gap-10">
                    <Image
                      src={product.image}
                      alt={product.title}
                      width={50}
                      height={50}
                    />
                    <h1 className="text-left">{product.title}</h1>
                  </div>
                </Link>
                <div className=" w-80 max-sm:w-20">
                  <h1>$ {product.price}</h1>
                </div>
                <div className=" w-80 max-sm:w-20  flex items-center justify-center gap-x-5">
                  <Button
                    name={"-"}
                    buttonStyles={
                      "rounded-3xl font-bold text-white  bg-orange-300 hover:bg-orange-400 focus:ring-4 focus:outline-none focus:bg-orange-300 font-medium rounded-lg text-sm px-4 py-2 text-center mr-3 md:mr-0 bg-orange-300 hover:bg-orange-400 dark:focus:bg-orange-400  transition-all ease-in-out duration-300"
                    }
                    handleClick={() =>
                      dispatch(decrementQuantityAction(product?.id))
                    }
                  />
                  <div>
                    <h1>{product?.quantity}</h1>
                  </div>
                  <Button
                    name={"+"}
                    buttonStyles={
                      "rounded-3xl font-bold text-white  bg-orange-300 hover:bg-orange-400 focus:ring-4 focus:outline-none focus:bg-orange-300 font-medium rounded-lg text-sm px-4 py-2 text-center mr-3 md:mr-0 bg-orange-300 hover:bg-orange-400 dark:focus:bg-orange-400  transition-all ease-in-out duration-300"
                    }
                    handleClick={() =>
                      dispatch(incrementQuantityAction(product?.id))
                    }
                  />
                </div>
                <div className=" w-80 max-sm:w-20">
                  ${product?.quantity * product?.price}
                </div>
              </div>
            );
          })}
          <div className="flex w-full justify-center mt-10">
            <h2 className="text-center text-3xl text-black">
              Grand Total: $<span className="underline">{getTotalPrice()}</span>
            </h2>
          </div>
        </div>
      )}
    </div>
  );
};

export default Cart;
