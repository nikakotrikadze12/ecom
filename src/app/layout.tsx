"use client";
import "./globals.css";
import Navbar from "@/components/Navbar/Navbar";
import Footer from "@/components/Footer/Footer";
import { Inter } from "next/font/google";
import store from "@/redux/redux";
import { Provider } from "react-redux/es/exports";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "eCom | Start your journey here",
  description: "Discover the best products on the market",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  let persistor = persistStore(store);
  return (
    <html lang="en" className="select-none">
      <Provider store={store}>
        <body className={inter.className}>
          <style>
            {`
            img {
              -moz-user-select: none;
              -webkit-user-select: none;
              -ms-user-select: none;
              user-select: none;
              -webkit-user-drag: none;
              user-drag: none;
              -webkit-touch-callout: none;
            }
            `}
          </style>
          <main>
            <PersistGate persistor={persistor}>
              <Navbar />
              {children}
              <Footer />
            </PersistGate>
          </main>
        </body>
      </Provider>
    </html>
  );
}
