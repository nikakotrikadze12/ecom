"use client";
import Catalogue from "@/components/Catalogue/Catalogue";
import Hero from "@/components/Hero/Hero";
import { useEffect } from "react";

export default function Main() {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }, []);
  return (
    <main>
      <Hero />
      <Catalogue />
    </main>
  );
}
