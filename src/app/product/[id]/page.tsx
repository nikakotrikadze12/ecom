"use client";
import React, { useEffect } from "react";
import Products from "@/components/DataFetching/DataFetching";
import Image from "next/image";
import Button from "@/components/Button/Button";
import { addToCart } from "@/redux/cart.slice";
import { useDispatch } from "react-redux";
import Link from "next/link";

const Page = ({ params }: { params: { id: any } }) => {
  const productId = params.id;
  const products = Products();
  const dispatch = useDispatch();

  const product = products.find((item) => item.id === parseInt(productId));

  const rater = Math.round(product?.rating?.rate ?? 0);
  const starCount = !isNaN(rater) && rater > 0 ? rater : 0;

  const randomBottomProducts = products.sort(() => 0.5 - Math.random());

  if (!product) {
    return (
      <div className="pt-44 pb-32 text-black shadow-md">
        <div className="flex justify-center flex-wrap gap-5">
          <div className="animate-pulse shadow-2xl w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 px-4">
            <div className="w-full h-96 bg-gray-300 rounded flex items-center justify-center overflow-hidden"></div>
          </div>
          <div className="flex flex-col items-center justify-evenly gap-5 w-full sm:w-auto px-4">
            <div className="w-full sm:w-96">
              <div className="h-2.5 bg-gray-200 rounded-full mb-4"></div>
              <div className="h-12 bg-gray-200 rounded-full mb-2.5"></div>
              <div className="h-2 bg-gray-200 rounded-full mb-2.5"></div>
              <div className="h-2 bg-gray-200 rounded-full mb-2.5"></div>
              <div className="h-2 bg-gray-200 rounded-full mb-2.5"></div>
              <div className="h-2 bg-gray-200 rounded-full"></div>
            </div>
            <Button
              name={"ADD TO CART"}
              buttonStyles={
                "bg-orange-500 w-full h-10 text-center text-white font-semibold transition-all ease-in-out duration-300 hover:bg-orange-600"
              }
              handleClick={() => dispatch(addToCart(products))}
            />
          </div>
        </div>
      </div>
    );
  }

  return (
    <>
      <div className="pt-44 pb-32 text-black shadow-md">
        <div className="flex justify-center flex-wrap gap-5">
          <div className=" shadow-2xl w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 px-4">
            <div className="w-full h-full flex items-center justify-center overflow-hidden">
              <Image
                src={product?.image}
                alt={product?.title}
                width={200}
                height={300}
              />
            </div>
          </div>
          <div className="flex flex-col items-center justify-evenly gap-5 w-full sm:w-auto px-4">
            <div className="w-full sm:w-96 flex flex-col gap-y-3">
              <h1 className="text-md font-light">{product?.category}</h1>
              <h1 className="text-4xl">{product?.title}</h1>
              <div className="flex gap-2">
                <div className="flex items-center">
                  {Array.from({ length: starCount }).map((_, index) => (
                    <svg
                      key={index}
                      aria-hidden="true"
                      className="w-5 h-5 text-yellow-400"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                    </svg>
                  ))}
                </div>
                <h1 className="text-sm">({product?.rating?.count}) Reviews</h1>
              </div>
              <h1 className="text-md">${product?.price}</h1>
              <h1 className="text-sm font-light">{product?.description}</h1>
            </div>
            <Link href={"/cart"} className="w-full">
              <Button
                name={"ADD TO CART"}
                buttonStyles={
                  "bg-orange-500 w-full h-10 text-center text-white font-semibold transition-all ease-in-out duration-300 hover:bg-orange-600"
                }
                handleClick={() => dispatch(addToCart(product))}
              />
            </Link>
          </div>
        </div>
        <div className="flex flex-col justify-center items-center text-center mt-40">
          <h1 className=" text-3xl text-black font-semibold">
            Explore More Products like this
          </h1>
          <div className="mt-20 flex  gap-10 justify-center">
            {randomBottomProducts.slice(0, 5).map((item) => {
              return (
                <div
                  className=" text-start w-48 flex flex-col justify-start"
                  key={item.id}
                >
                  <Link
                    href={`/product/${item?.id}`}
                    target="_blank"
                    className="flex flex-col items-center"
                  >
                    <Image
                      src={item?.image?.toString()}
                      alt={item?.title?.toString()}
                      className="h-32"
                      width={100}
                      height={100}
                    />

                    <h1 className="font-bold text-orange-500 mt-3 mb-3">
                      ${item?.price}
                    </h1>
                    <h1>{item?.title}</h1>
                  </Link>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};

export default Page;
