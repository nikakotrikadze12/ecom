"use client";
import React from "react";
import { buttonProps } from "@/types";

const Button = ({ name, buttonStyles, handleClick }: buttonProps) => {
  return (
    <button type={"button"} onClick={handleClick} className={`${buttonStyles}`}>
      {name}
    </button>
  );
};

export default Button;
