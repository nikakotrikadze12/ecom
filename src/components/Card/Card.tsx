import React from "react";
import { cardProps } from "@/types";
import Image from "next/image";
import Button from "../Button/Button";
import Link from "next/link";
import { addToCart } from "@/redux/cart.slice";
import { useDispatch } from "react-redux";

const Card = ({
  title,
  description,
  price,
  imageSrc,
  index,
  product,
}: cardProps) => {
  const dispatch = useDispatch();

  return (
    <div className="flex flex-col justify-evenly items-center w-64 gap-y-1 shadow-2xl bg-white">
      <Link href={`/product/${index}`} target="_blank">
        <div className="flex items-center justify-center pt-5 pb-5 h-60 w-full overflow-hidden">
          <Image src={imageSrc} alt="title" width={100} height={50} />
        </div>
        <div className="h-28 flex-col ">
          <h1 className="text-center">
            {title.length >= 25
              ? title.slice(0, 25).trim() + "..."
              : title.trim()}
          </h1>
          <p className="pt-5 text-xs text-center text-stone-500 w-60">
            {description.length >= 60
              ? description.slice(0, 60).trim() + "..."
              : description.trim()}
          </p>
        </div>
        <h1 className="text-center ">${price}</h1>
      </Link>
      <Link href={"/cart"} className="w-full">
        <Button
          name={"ADD TO CART"}
          buttonStyles={
            "bg-orange-500 w-full h-10 text-center text-white font-semibold transition-all ease-in-out duration-300 hover:bg-orange-600"
          }
          handleClick={() => dispatch(addToCart(product))}
        />
      </Link>
    </div>
  );
};

export default Card;
