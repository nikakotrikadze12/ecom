"use client";

import React, { useEffect, useState } from "react";
import Card from "../Card/Card";
import Products from "../DataFetching/DataFetching";

const Catalogue = () => {
  const products = Products();
  const [productCount, setProductCount] = useState(8);
  const loadMoreProducts = () => {
    setProductCount(productCount + 8);
  };
  useEffect(() => {
    console.log(products.length);
  }, []);
  return (
    <div className="pt-20 pb-20" id="catalogue">
      <div className=" pl-10 pb-20">
        <h1 className=" text-5xl max-md:text-4xl max-sm:text-3xl font-semibold">
          Catalogue
        </h1>
        <p className="text-sm mt-2">Explore some products you might like</p>
      </div>
      <div className="grid xl:grid-cols-4 max-xl:grid-cols-3 lg:grid-cols-3 max-lg:grid-cols-3 md:grid-cols-2 max-md:grid-cols-2 sm:grid-cols-1 max-sm:grid-cols-1 gap-y-20 place-items-center">
        {products.slice(0, productCount).map((product) => {
          return (
            <Card
              title={product.title}
              description={product.description}
              price={product.price}
              imageSrc={product.image}
              key={product.id}
              index={product.id}
              product={product}
            />
          );
        })}
      </div>
      {productCount <= 20 ? (
        <div className="flex w-full justify-end pt-10 pr-12">
          <button
            onClick={loadMoreProducts}
            className="p-3 rounded-full bg-orange-500 text-white  transition-all ease-in-out duration-300 hover:bg-orange-600"
          >
            Load More..
          </button>
        </div>
      ) : null}
    </div>
  );
};

export default Catalogue;
