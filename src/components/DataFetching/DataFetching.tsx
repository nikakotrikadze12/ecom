import { useEffect, useState } from "react";
import { productsProps } from "@/types";
import axios from "axios";

const Products = () => {
  const [products, setProducts] = useState<productsProps[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const apiKey = process.env.NEXT_PUBLIC_FAKESTORE_API_KEY;
        if (apiKey) {
          const response = await axios.get(apiKey);
          setProducts(response.data);
        } else {
          console.error("API key is undefined");
        }
      } catch (error) {
        console.error("Error fetching products:", error);
      }
    };

    fetchData();
  }, []);

  return products;
};

export default Products;
