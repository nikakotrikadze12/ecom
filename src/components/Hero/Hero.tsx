"use client";

import React from "react";
import Button from "../Button/Button";
import macMonitor from "../../assets/macMonitor.png";
import Image from "next/image";

const Hero = () => {
  const handleScroll = () => {
    const element = document.getElementById("catalogue");
    if (element) {
      element.scrollIntoView({ behavior: "smooth" });
    }
  };

  return (
    <div className="bg-orange-500 mt-20">
      <div className="flex flex-col lg:flex-row items-center justify-center lg:justify-evenly pt-20 pb-20">
        <div className="text-center lg:text-center space-y-20">
          <h1 className="text-white text-6xl max-md:text-5xl max-sm:text-4xl font-bold max-w-xl">
            Get yourself your favorite item from the store
          </h1>
          <Button
            name={"Explore Store"}
            buttonStyles="w-36 h-12 bg-orange-300 rounded-3xl font-bold text-white hover:scale-105 hover:bg-orange-400 transition-all ease-in-out duration-300"
            handleClick={handleScroll}
          />
        </div>
        <div className="flex items-center justify-center max-lg:mt-10 max-md:mt-10 max-sm:mt-10">
          <Image
            alt={"heromonitorImage"}
            src={macMonitor}
            className="h-auto lg:max-w-2xl max-lg:max-w-xl max-md:w-2/4 max-sm:w-2/3 rounded-full hover:-translate-y-1 transition-all duration-500"
          />
        </div>
      </div>
    </div>
  );
};

export default Hero;
