"use client";
import React, { useEffect } from "react";
import Image from "next/image";
import ecomLogo from "../../assets/ecomLogo.png";
import Button from "../Button/Button";
import Link from "next/link";
import Cart from "../Cart/Cart";

const Navbar = () => {
  return (
    <nav className="bg-white fixed w-full z-20 top-0 left-0 border-b border-gray-200 backdrop-filter backdrop-blur-sm">
      <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <Link href={"/"} className="flex justify-center items-center">
          <Image
            alt={"ecom-logo"}
            src={ecomLogo}
            width={50}
            height={50}
            className="select-none"
          />
          <span className="self-center text-2xl font-semibold whitespace-nowrap text-black">
            eCom
          </span>
        </Link>

        <div className="flex md:order-2 gap-5">
          <Button
            name={"Sign In"}
            buttonStyles={
              "rounded-3xl font-bold text-white  bg-orange-300 hover:bg-orange-400 focus:ring-4 focus:outline-none focus:bg-orange-300 font-medium rounded-lg text-sm px-4 py-2 text-center mr-3 md:mr-0 bg-orange-300 hover:bg-orange-400 dark:focus:bg-orange-400  transition-all ease-in-out duration-300"
            }
          />
          <Cart />
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
