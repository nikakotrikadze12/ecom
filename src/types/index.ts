import { MouseEventHandler } from "react";

export interface buttonProps {
  name: string;
  buttonStyles?: string;
  handleClick?: MouseEventHandler<HTMLButtonElement>;
}

export interface productsProps {
  title: string;
  id: any;
  description: string;
  price: number;
  image: HTMLImageElement;
  category: string;
  rating?: {
    count?: number;
    rate?: number;
  };
  quantity: number;
}

export interface cardProps {
  title: string;
  description: string;
  price: any;
  imageSrc: HTMLImageElement;
  index: number;
  product: productsProps;
}
